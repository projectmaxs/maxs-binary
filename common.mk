# Function that returns of directory of the currently parsed Makefile
# The idea is that $MAKEFILE_LIST gets extended by every Makefile that
# is included, the last included (=lastword) Makefile is the one we
# are currently parsing
makefile-dir = $(dir $(CURDIR)/$(lastword $(MAKEFILE_LIST)))

%.png: %.xcf
	$(call makefile-dir)/scripts/xcf2png.sh -f $<
